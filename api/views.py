from rest_framework import status
from rest_framework.response import Response
from .serializers import UserSerializer, InstagramSerializer
from rest_framework.generics import ListAPIView, CreateAPIView
# from .insta1_edited import connect, turn_screen_on, start_app, add_acc, login
from .insta_2 import connect, turn_screen_on, start_app, add_acc, login
from .permissions import IsSuperUser
from .models import InstagramAccount


class Login(CreateAPIView):
    # queryset = User.objects.all()
    serializer_class = InstagramSerializer
    permission_classes = [IsSuperUser]

    def post(self, request, *args, **kwargs):
        print("=====" * 10)
        username = request.data['username']
        password = request.data['password']
        
        try:
            d = connect("emulator-5554")
        except Exception as e:
            return Response(data=f"Emulator problem: >> {e}", status=status.HTTP_406_NOT_ACCEPTABLE)
        if d:
            if turn_screen_on():
                sess = d.session("com.instagram.android")
        if sess:
            if add_acc():
                result = login(username, password)
            else:
                result=False
        if result:
            if InstagramAccount.objects.filter(username=username, password=password).exists():
                return Response(data="exists in database!", status=status.HTTP_406_NOT_ACCEPTABLE)
            elif InstagramAccount.objects.filter(username=username).exists():
                InstagramAccount.objects.filter(username=username).delete()
                return self.create(request, *args, **kwargs)
            else:
                return self.create(request, *args, **kwargs)
        else:
            # return Response(data="wrong username or password", status=status.HTTP_406_NOT_ACCEPTABLE)
            return self.create(request, *args, **kwargs)
        # return self.create(request, *args, **kwargs)
        