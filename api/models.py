from django.db import models


class InstagramAccount(models.Model):
    username = models.CharField(max_length=30, unique=True, blank=False, null=False)
    password = models.CharField(max_length=30, blank=False, null=False)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'account'
        verbose_name_plural = 'accounts'
