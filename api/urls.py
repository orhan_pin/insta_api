from django.urls import path
from .views import Login

app_name = 'api'

urlpatterns = [
    path('1/', Login.as_view(), name='Login'),
]
