from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import InstagramAccount


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = '__all__'


class InstagramSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstagramAccount
        fields = '__all__'
