import uiautomator2 as u2
from uiautomator2 import exceptions
import time as t
import os

file_path = os.path.dirname(__file__)


def connect(device):
    try:
        global d
        d = u2.connect_usb(device)
        d.settings['wait_timeout'] = 10
        return d
    except u2.exceptions.ConnectError as e:
        return False


# Turn the screen on
def turn_screen_on():
    if not d.info["screenOn"]:
        d.screen_on()
    return True


# com.instagram.android
# Start the app and wait until the app opens completely
def start_app():
    global sess
    # d.app_start("com.instagram.android", use_monkey=True)
    # sess.app_wait("com.instagram.android")
    sess = d.session("com.instagram.android")
    sess.app_wait("com.instagram.android")
    # t.sleep(1.3)
    return True


# Add account
def add_acc():
    try:
        d.xpath('//*[@resource-id="com.instagram.android:id/profile_tab"]/android.view.ViewGroup[1]').long_click()
        d(text="Add account").click()
        d(text="Log In to Existing Account").click()
        # d.xpath('//*[@resource-id="com.instagram.android:id/primary_button"]').click()
        return True
    except Exception as e:
        print(e)
        return False


# Login
def login(username, password):
    # Get username
    # username = input("username: ")
    # # Get password
    # password = input("password: ")

    if username and password:
        d.xpath('//*[@resource-id="com.instagram.android:id/login_username"]').set_text(username)
        d.xpath('//*[@resource-id="com.instagram.android:id/password_input_layout"]').set_text(password)
        d.xpath('//*[@resource-id="com.instagram.android:id/button_text"]').click()
        print("logging in")
        if d.wait_activity("com.instagram.mainactivity.MainActivity", 6):
            print("logged in successfully")
            d.xpath('//*[@resource-id="android:id/autofill_save_no"]').click()
            return True
        else:
            print("Error: maybe wrong user or password")
            return False
